import path from "path";

const targetAddress = new URL(
  process.env.TARGET_ADDRESS || "http://localhost:8000",
);

export default {
  siteMetadata: {
    baseUrl: targetAddress.href,
    translations: ["id"],
    lang: "en-US",
    title: "Holy Shirts & Pants",
    titleTemplate: "%s | Holy Shirts & Pants",
    description: "Create a wardrobe of biblical proportions.",
    author: "info@example.com",
    copyright: "© 2020 Holy Shirts & Pants",
    image: "https://source.unsplash.com/8fwZP4bq2bM/2000x1322",
    social: {
      twitterUsername: "@holyshirtspants",
      instagramUsername: "@holyshirtspants",
      telegramChannel: "@holyshirtspants",
    },
    robots: "index, follow",
    keywords: ["ppc", "marketing", "target", "search phraes"],
    forms: {
      emailSubscription: process.env.GETFORM_EMAIL_SUBSCRIPTION_ENDPOINT,
    },
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-theme-shopify-manager",
      options: {
        shopName: process.env.SHOPIFY_SHOP_NAME,
        accessToken: process.env.SHOPIFY_ACCESS_TOKEN,
      },
    },
    "gatsby-transformer-sharp",
    "gatsby-plugin-sharp",
    {
      resolve: "gatsby-plugin-s3",
      options: {
        bucketName: process.env.AWS_TARGET_BUCKET_NAME || "fake-bucket",
      },
    },
    "gatsby-plugin-catch-links",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: path.resolve("src/pages/"),
      },
    },
    {
      resolve: "gatsby-plugin-mdx",
      options: {
        defaultLayouts: {
          default: require.resolve("../src/components/MDXLayout.tsx"),
        },
      },
    },
  ],
};
