import React from "react";
import { navigate } from "@reach/router";

import { ChakraProvider, CSSReset } from "@chakra-ui/core";
import { Global } from "@emotion/core";

import { I18nextProvider } from "react-i18next";
import i18next from "../src/lib/i18next";

import theme from "../src/utils/theme";
import { globalStyles } from "../src/utils/styles";

import { Auth0Provider } from "@auth0/auth0-react";
const onRedirectCallback = (appState) => {
  navigate(appState?.returnTo || "/", { replace: true });
};
const authConfig = {
  domain: process.env.GATSBY_AUTH0_DOMAIN,
  clientId: process.env.GATSBY_AUTH0_CLIENT_ID,
  redirectUri: process.env.GATSBY_AUTH0_REDIRECT_URI,
  onRedirectCallback,
};

export const wrapRootElement = ({ element }) => (
  <ChakraProvider theme={theme}>
    <Global styles={globalStyles} />
    <CSSReset />
    <I18nextProvider i18n={i18next}>
      <Auth0Provider {...authConfig}>{element}</Auth0Provider>
    </I18nextProvider>
  </ChakraProvider>
);
