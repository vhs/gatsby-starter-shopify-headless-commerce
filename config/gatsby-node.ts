import fs from "fs-extra";
import path from "path";

/**
 * Automatically redirect users to the password
 * page when deployed to S3 with CloudFront.
 */
const siteRedirects = [
  {
    fromPath: "/",
    toPath: "/password",
    redirectInBrowser: false,
    isPermanent: false,
  },
];

/**
 * Copy i18next translation files.
 */
export const onPostBuild = ({ reporter }) => {
  reporter.info("copy translation files");
  fs.copySync(
    path.join(__dirname, "../src/locales"),
    path.join(__dirname, "../public/locales"),
  );
};

export const createPages = async ({ actions, graphql, reporter }) => {
  const { createPage, createRedirect } = actions;

  /**
   * Create redirects.
   */
  siteRedirects.forEach((redirect) => createRedirect(redirect));

  /**
   * Create details pages for all Shopify products by product handle.
   * @throws Error and fails build on bad query result.
   */
  const shopifyProductQueryResult = await graphql(`
    query {
      allShopifyProduct {
        edges {
          node {
            handle
          }
        }
      }
    }
  `);
  if (shopifyProductQueryResult.errors) {
    reporter.panicOnBuild(
      `failed to process products for "createPages" query`,
      shopifyProductQueryResult.errors,
    );
    throw new Error(shopifyProductQueryResult.errors);
  }
  shopifyProductQueryResult.data.allShopifyProduct.edges.forEach(({ node }) => {
    createPage({
      path: `/product/${node.handle}/`,
      component: require.resolve(`../src/templates/ProductDetails.tsx`),
      context: {
        handle: node.handle,
      },
    });
  });
};
