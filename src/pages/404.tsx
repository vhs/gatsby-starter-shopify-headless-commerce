import React, { FC } from "react";
import { PageProps } from "gatsby";
import { useTranslation } from "react-i18next";
import { Heading, Text } from "@chakra-ui/core";

import Layout from "../components/Layout";
import SEO from "../components/SEO";

const NotFoundPage: FC<PageProps> = () => {
  const { t } = useTranslation();
  return (
    <Layout>
      <SEO title={t("pages.404.title")} robots="noindex, nofollow" />
      <Heading>{t("pages.404.heading")}</Heading>
      <Text>{t("pages.404.notFoundText")}</Text>
    </Layout>
  );
};

export default NotFoundPage;
