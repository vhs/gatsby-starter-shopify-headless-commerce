import React, { FC } from "react";
import { graphql, PageProps } from "gatsby";
import { Heading, Text } from "@chakra-ui/core";
import { useTranslation } from "react-i18next";

import Layout from "../components/Layout";
import SEO from "../components/SEO";
import ProductGrid from "../components/ProductGrid";

interface IndexPageProps extends PageProps {
  data: {
    siteBuildMetadata: {
      buildTime: string;
    };
  };
}

export const pageQuery = graphql`
  query IndexQuery {
    siteBuildMetadata {
      buildTime(formatString: "dddd, MMMM Do YYYY, h:mm:ss a")
    }
  }
`;

const IndexPage: FC<IndexPageProps> = ({ data, path }) => {
  const { buildTime } = data.siteBuildMetadata;
  const { t } = useTranslation();
  return (
    <Layout>
      <SEO />
      <Heading as="h1" fontWeight={400} mb={6}>
        {t("pages.homepage.heading.featured")}
      </Heading>
      <ProductGrid />
      <Text mt={16}>{t("lastUpdated", { buildTime })}</Text>
    </Layout>
  );
};

export default IndexPage;
