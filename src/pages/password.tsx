import React, { FC, SyntheticEvent, useState } from "react";
import { navigate, useLocation } from "@reach/router";
import {
  Stack,
  Box,
  Button,
  Heading,
  Text,
  FormControl,
  FormLabel,
  Input,
  VisuallyHidden,
  InputGroup,
  InputRightElement,
  Wrap,
  AlertIcon,
  Alert,
  Container,
  DarkMode,
} from "@chakra-ui/core";
import { FaFacebook, FaTwitter, FaPinterest } from "react-icons/fa";
import { useSiteMetadata } from "../hooks/useSiteMetadata";
import { useAuth0 } from "@auth0/auth0-react";

import SEO from "../components/SEO";
import ErrorAlert from "../components/ErrorAlert";

const handleSocialShare = (event: SyntheticEvent<HTMLButtonElement>) => {
  globalThis.open(event.currentTarget.dataset.sharelink);
};

const PasswordPage: FC = () => {
  const {
    title: siteTitle,
    baseUrl: shareUrl,
    forms: { emailSubscription: formAction },
  } = useSiteMetadata();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [wasSubmittedSuccessfully, setWasSubmittedSuccessfully] = useState(
    false,
  );
  const { href: returnTo } = useLocation();
  const { isLoading, error, isAuthenticated, loginWithRedirect } = useAuth0();

  const handleEnterButtonClick = (event: SyntheticEvent<HTMLButtonElement>) => {
    if (isAuthenticated) {
      navigate("/");
    } else {
      loginWithRedirect({ appState: { returnTo } });
    }
  };

  const handleFormSubmit = async (event: SyntheticEvent<HTMLFormElement>) => {
    event.preventDefault();

    // sanitize form data and prevent duplicates
    const formData = new FormData(event.currentTarget);
    for (const [key, value] of formData.entries()) {
      // only allow emails with a value
      if (value && key === "email") {
        // rename form field before posting
        formData.set("launch_email", value);
      }
      formData.delete(key); // scrub any other data
    }
    if (!formData.has("launch_email")) return;

    setIsSubmitting(true);

    const { action, method } = event.currentTarget;
    await fetch(action, { method, body: formData })
      .then((response: Response) => setWasSubmittedSuccessfully(response.ok))
      .catch((error) => console.log(error));

    setIsSubmitting(false);
  };

  return (
    <>
      <SEO title="Coming Soon" robots="noindex, nofollow" />
      {error && <ErrorAlert message={error.message} />}
      <Stack spacing={8}>
        <DarkMode>
          <Button
            alignSelf="flex-end"
            my={5}
            mx={8}
            letterSpacing={2}
            fontSize="sm"
            color={isAuthenticated ? "purple.200" : "inherit"}
            variant="outline"
            isDisabled={isLoading}
            onClick={handleEnterButtonClick}
            textTransform="uppercase"
          >
            {isAuthenticated ? "Continue to site ➙" : "Enter using password ➙"}
          </Button>
        </DarkMode>
        <Box alignSelf="center" textAlign="center" maxW="75vw">
          <Heading as="h1">{siteTitle}</Heading>
          <Text
            fontSize="xl"
            fontWeight={500}
            letterSpacing={3}
            mt={3}
            textTransform="uppercase"
          >
            Coming soon
          </Text>
          <Text fontWeight="bold" mt={20} fontSize="lg" letterSpacing="1px">
            Be the first to know when we launch.
          </Text>
          <Container>
            <Text mt={4}>
              Promotions, new products and sales. Directly to your inbox.
            </Text>
          </Container>
          <Alert
            status="success"
            variant="left-accent"
            mt={8}
            display={wasSubmittedSuccessfully ? "block" : "none"}
          >
            {/* TODO: Remove display hack with Chakra upgrade */}
            <AlertIcon display="inline-block" />
            We will send you an email right before we open!
          </Alert>
          <form method="post" action={formAction} onSubmit={handleFormSubmit}>
            <Stack spacing={5} mt={[4, 6, 8]}>
              <FormControl
                isDisabled={wasSubmittedSuccessfully}
                isRequired
                display={["none", "inherit"]}
              >
                <VisuallyHidden>
                  <FormLabel>Email address</FormLabel>
                </VisuallyHidden>
                <InputGroup maxW="560px" width="80vw">
                  <Input
                    type="email"
                    name="email"
                    placeholder="Email address"
                  />
                  <InputRightElement
                    width="9rem"
                    mr={wasSubmittedSuccessfully ? "-16px" : "-20px"}
                  >
                    <Button
                      type="submit"
                      colorScheme="purple"
                      borderLeftRadius={0}
                      isLoading={isSubmitting}
                      isDisabled={isSubmitting || wasSubmittedSuccessfully}
                      textTransform="uppercase"
                    >
                      {wasSubmittedSuccessfully ? "Subscribed" : "Notify me"}
                    </Button>
                  </InputRightElement>
                </InputGroup>
              </FormControl>
              <FormControl
                isDisabled={wasSubmittedSuccessfully}
                isRequired
                display={["inherit", "none"]}
                width="75vw"
                alignSelf="center"
              >
                <VisuallyHidden>
                  <FormLabel>Email address</FormLabel>
                </VisuallyHidden>
                <Input type="email" name="email" placeholder="Email address" />
              </FormControl>
              <Button
                type="submit"
                display={["inherit", "none"]}
                colorScheme="green"
                width="75vw"
                alignSelf="center"
                isLoading={isSubmitting}
                isDisabled={isSubmitting || wasSubmittedSuccessfully}
                textTransform="uppercase"
              >
                {wasSubmittedSuccessfully ? "Subscribed" : "Notify me"}
              </Button>
            </Stack>
          </form>
          <Text fontSize="lg" fontWeight="bold" mt={[16, 24, 24]}>
            Spread the word
          </Text>
          <Wrap justify="center" my={12}>
            <Button
              colorScheme="facebook"
              variant="outline"
              leftIcon={<FaFacebook />}
              data-sharelink={`https://www.facebook.com/sharer.php?u=${shareUrl}`}
              onClick={handleSocialShare}
              textTransform="uppercase"
              size="sm"
            >
              Share
            </Button>
            <Button
              colorScheme="twitter"
              variant="outline"
              leftIcon={<FaTwitter />}
              data-sharelink={`https://twitter.com/share?text=&url=${shareUrl}`}
              onClick={handleSocialShare}
              textTransform="uppercase"
              size="sm"
            >
              Tweet
            </Button>
            <Button
              colorScheme="pinterest"
              variant="outline"
              leftIcon={<FaPinterest />}
              data-sharelink={`https://pinterest.com/pin/create/button/?url=${shareUrl}&media=&description=`}
              onClick={handleSocialShare}
              textTransform="uppercase"
              size="sm"
            >
              Pin It
            </Button>
          </Wrap>
        </Box>
      </Stack>
    </>
  );
};

export default PasswordPage;
