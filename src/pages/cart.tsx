import React, { FC } from "react";
import { PageProps } from "gatsby";
import {
  Heading,
  Button,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  Stack,
} from "@chakra-ui/core";

import { useCart, useCartItems } from "gatsby-theme-shopify-manager";
import { useTranslation } from "react-i18next";

import Layout from "../components/Layout";
import SEO from "../components/SEO";
import CartItems from "../components/CartItems";
import { formatCurrency } from "../utils/format";

const CartPage: FC<PageProps> = () => {
  const shopifyCart = useCart();
  const shopifyCartItems = useCartItems();
  const { t, i18n } = useTranslation();
  const getPrice = (price: string) =>
    formatCurrency(price, i18n.language, shopifyCart?.currencyCode);

  return (
    <Layout>
      <SEO title={t("pages.cart.title")} robots="noindex" />
      <Heading as="h1" fontWeight={400} mb={6}>
        {t("pages.cart.heading")}
      </Heading>
      <CartItems lineItems={shopifyCartItems || []} />
      <Stack spacing={3} my={6}>
        <Stat>
          <StatLabel>{t("cart.statSubtotal")}</StatLabel>
          <StatNumber>{getPrice(shopifyCart?.subtotalPrice)}</StatNumber>
        </Stat>
        <Stat>
          <StatLabel>{t("cart.statTaxes")}</StatLabel>
          <StatNumber>{getPrice(shopifyCart?.totalTax)}</StatNumber>
        </Stat>
        <Stat>
          <StatLabel>{t("cart.statTotal")}</StatLabel>
          <StatNumber>{getPrice(shopifyCart?.totalPrice)}</StatNumber>
          <StatHelpText>{t("stat.plusShipping")}</StatHelpText>
        </Stat>
      </Stack>
      <Button minW={40} colorScheme="green" mb={6}>
        {t("cart.buttonCheckout")}
      </Button>
    </Layout>
  );
};

export default CartPage;
