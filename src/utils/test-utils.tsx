import React, { FC, ReactElement } from "react";
import { render } from "@testing-library/react";

import { LocationProvider } from "@reach/router";
import { ChakraProvider } from "@chakra-ui/core";
import theme from "./theme";

import { I18nextProvider } from "react-i18next";
import i18next from "../lib/i18next";

const Providers: FC = ({ children }) => {
  return (
    <LocationProvider>
      <ChakraProvider theme={theme}>
        <I18nextProvider i18n={i18next}>{children}</I18nextProvider>
      </ChakraProvider>
    </LocationProvider>
  );
};

const customRender = (ui: ReactElement, options: object = {}) =>
  render(ui, { wrapper: Providers, ...options });

export * from "@testing-library/react";
export { customRender as render };
