export const formatCurrency = (
  price: string,
  locale?: string | undefined,
  currencyCode?: string,
) => {
  return Intl.NumberFormat(locale, {
    style: "currency",
    currency: currencyCode ? currencyCode : "USD",
    minimumFractionDigits: 2,
  }).format(parseFloat(price ? price : "0"));
};
