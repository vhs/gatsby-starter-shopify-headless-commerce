import { graphql } from "gatsby";

export const globalImageQueries = graphql`
  fragment ProductImageFluid on ImageSharp {
    fluid(traceSVG: { color: "#9f7aea", blackOnWhite: false }) {
      ...GatsbyImageSharpFluid_withWebp_tracedSVG
    }
  }

  fragment ProductImageThumbFluid on ImageSharp {
    fluid(maxWidth: 120, traceSVG: { color: "#9f7aea", blackOnWhite: false }) {
      ...GatsbyImageSharpFluid_withWebp_tracedSVG
    }
  }
`;
