import theme, { Theme } from "@chakra-ui/theme";
import { mode, Styles } from "@chakra-ui/theme-tools";

const styles: Styles = {
  ...theme.styles,
  global: (props) => ({
    ...theme.styles.global,
    fontFamily: "body",
    fontWeight: "light",
    color: mode("gray.100", "whiteAlpha.900")(props),
    bg: mode("gray.700", "gray.900")(props),
    "a:link:not(.navlink)": {
      color: mode("purple.300", "purple.200")(props),
    },
    "a:visited:not(.navlink)": {
      color: mode("purple.300", "purple.200")(props),
    },
    "a:active:not(.navlink)": {
      color: mode("purple.500", "purple.400")(props),
    },
    ".product-description p": {
      marginBottom: "1em",
    },
  }),
};

const customTheme: Theme = {
  ...theme,
  fonts: {
    ...theme.fonts,
    body: `"Source Sans Pro",${theme.fonts.body}`,
    heading: `"Source Sans Pro",${theme.fonts.heading}`,
  },
  colors: {
    ...theme.colors,
    // green: "#00991b",
    // green: "#56b038",
    // black: "#131217",
    black: "#020202",
  },
  // components: {
  //   ...theme.components,
  //   Divider: {
  //     ...theme.components.Divider,
  //     baseStyle: {
  //       color: "#696969",
  //     },
  //   },
  // },
  config: {
    ...theme.config,
    useSystemColorMode: false,
    initialColorMode: "dark",
  },
  styles,
};

export default customTheme;
