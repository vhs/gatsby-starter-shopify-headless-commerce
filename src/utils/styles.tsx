import { css } from "@emotion/core";

import typefaceCrimsonText from "../fonts/CrimsonText-Regular.woff2";
import typefaceCrimsonTextSemiBold from "../fonts/CrimsonText-SemiBold.woff2";
import typefaceSourceSansProLight from "../fonts/SourceSansPro-Light.woff2";
import typefaceSourceSansPro from "../fonts/SourceSansPro-Regular.woff2";

const webfonts = css`
  @font-face {
    font-family: "Crimson Text";
    font-style: normal;
    font-weight: 400;
    font-display: fallback;
    src: local("CrimsonText Regular"), local("CrimsonText-Regular"),
      url(${typefaceCrimsonText}) format("woff2");
    unicode-range: U+0100-024f, U+1-1eff, U+20a0-20ab, U+20ad-20cf, U+2c60-2c7f,
      U+A720-A7FF;
  }
  @font-face {
    font-family: "Crimson Text";
    font-style: normal;
    font-stretch: normal;
    font-weight: 600;
    font-display: fallback;
    src: local("CrimsonText SemiBold"), local("CrimsonText-SemiBold"),
      url(${typefaceCrimsonTextSemiBold}) format("woff2");
    unicode-range: U+0100-024f, U+1-1eff, U+20a0-20ab, U+20ad-20cf, U+2c60-2c7f,
      U+A720-A7FF;
  }
  @font-face {
    font-family: "Source Sans Pro";
    font-style: normal;
    font-stretch: normal;
    font-weight: 300;
    font-display: fallback;
    src: local("SourceSansPro Light"), local("SourceSansPro-Light"),
      url(${typefaceSourceSansProLight}) format("woff2");
    unicode-range: U+0100-024f, U+1-1eff, U+20a0-20ab, U+20ad-20cf, U+2c60-2c7f,
      U+A720-A7FF;
  }
  @font-face {
    font-family: "Source Sans Pro";
    font-style: normal;
    font-weight: 400;
    font-display: fallback;
    src: local("SourceSansPro Regular"), local("SourceSansPro-Regular"),
      url(${typefaceSourceSansPro}) format("woff2");
    unicode-range: U+0100-024f, U+1-1eff, U+20a0-20ab, U+20ad-20cf, U+2c60-2c7f,
      U+A720-A7FF;
  }
`;

export { webfonts as globalStyles };
