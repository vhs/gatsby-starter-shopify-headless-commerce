import React, { FC, useState, useRef, useCallback, useEffect } from "react";
import find from "lodash/find";
import isEqual from "lodash/isEqual";

import { MinusIcon, AddIcon } from "@chakra-ui/icons";
import { FiShoppingCart } from "react-icons/fi";
import {
  Button,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  FormControl,
  FormLabel,
  DarkMode,
  Select,
  Box,
} from "@chakra-ui/core";

import {
  useAddItemToCart,
  useClientUnsafe,
} from "gatsby-theme-shopify-manager";
import { useTranslation } from "react-i18next";
import { useToast } from "@chakra-ui/core";

import ProductPrice from "./ProductPrice";

interface ShopifyProductOption {
  name: string;
  value: string;
}

interface ShopifyProductVariant {
  title: string;
  priceV2: {
    amount: string;
    currencyCode: string;
  };
  compareAtPrice: string;
  availableForSale: boolean;
  shopifyId: string;
  selectedOptions: ShopifyProductOption[];
}

export interface IShopifyProduct {
  availableForSale: boolean;
  shopifyId: string;
  options: {
    id: string;
    name: string;
    values: string[];
  }[];
  variants: ShopifyProductVariant[];
}

interface ProductFormProps {
  shopifyProduct: IShopifyProduct;
}

const hasAvailabilityForOptions = (
  variants: ShopifyProductVariant[],
  options: ShopifyProductOption[],
): boolean =>
  getVariantForOptions(variants, options)?.availableForSale || false;

const getVariantForOptions = (
  variants: ShopifyProductVariant[],
  options: ShopifyProductOption[],
): ShopifyProductVariant | undefined =>
  find(variants, ({ selectedOptions }) => isEqual(options, selectedOptions));

const ProductForm: FC<ProductFormProps> = ({ shopifyProduct }) => {
  const {
    options,
    variants,
    variants: [firstVariant],
  } = shopifyProduct;

  const addItemToCart = useAddItemToCart();

  const [variant, setVariant] = useState<ShopifyProductVariant>(firstVariant);
  const [isAddingToCart, setIsAddingToCart] = useState(false);
  const [hasConfirmedAvailability, setHasConfirmedAvailability] = useState(
    false,
  );
  const quantityRef = useRef(1);

  const { t } = useTranslation();
  const toast = useToast();

  const handleAddToCartClick = async () => {
    try {
      setIsAddingToCart(true);
      await addItemToCart(variant.shopifyId, quantityRef.current);
      toast({
        title: t("productForm.addedToCart.success.title"),
        description: t("productForm.addedToCart.success.description", {
          quantity: quantityRef.current,
        }),
        status: "success",
        isClosable: true,
      });
    } catch {
      toast({
        title: t("productForm.addedToCart.error.title"),
        description: t("productForm.addedToCart.error.description"),
        status: "error",
        duration: 9000,
        isClosable: true,
      });
    } finally {
      setIsAddingToCart(false);
    }
  };

  const handleQuantityChange = (selectedQuantity: number) => {
    if (selectedQuantity !== quantityRef.current) {
      quantityRef.current = isNaN(selectedQuantity) ? 0 : selectedQuantity;
    }
  };

  const handleOptionChange = (
    optionIndex: number,
    { target: { value } }: { target: { value: string } },
  ) => {
    const currentOptions = [...variant.selectedOptions];
    currentOptions[optionIndex] = {
      ...currentOptions[optionIndex],
      value,
    };
    const selectedVariant = getVariantForOptions(variants, currentOptions);
    if (selectedVariant) {
      setVariant(selectedVariant);
    } else {
      const errorOptions = JSON.stringify(currentOptions);
      throw new Error(`Unable to locate item with options: ${errorOptions}`);
    }
  };

  const shouldDisableOption = (option: ShopifyProductOption) => {
    return !hasAvailabilityForOptions(variants, [option]);
  };

  const shopifyClient = useClientUnsafe();
  const confirmAvailability = useCallback(
    (productId) => {
      shopifyClient.product
        .fetch(productId)
        .then(
          (fetchedProduct: {
            variants: { id: string; available: boolean }[];
          }) => {
            const fetchedVariant = fetchedProduct.variants.find(
              (fetchedVariant) => fetchedVariant.id === variant.shopifyId,
            );
            fetchedVariant &&
              setHasConfirmedAvailability(fetchedVariant.available);
          },
        );
    },
    [shopifyClient?.product],
  );

  useEffect(() => {
    confirmAvailability(shopifyProduct.shopifyId);
  }, [variant]);

  return (
    <>
      <Box my={4}>
        <ProductPrice
          price={variant.priceV2.amount}
          currencyCode={variant.priceV2.currencyCode}
          compareAtPrice={variant.compareAtPrice}
        />
      </Box>
      <DarkMode>
        {options.map(({ id, name, values }, index) => (
          <FormControl key={id} id={name} isRequired mb={4}>
            <FormLabel>
              {t(`__GRAPHQL__.options.${name.toLocaleLowerCase()}`)}
            </FormLabel>
            <Select
              onChange={(event) => handleOptionChange(index, event)}
              variant="filled"
              isDisabled={!shopifyProduct.availableForSale}
            >
              {values.map((value) => (
                <option
                  key={`${name}-${value}`}
                  style={{ backgroundColor: "#131217" }}
                  value={value}
                  disabled={shouldDisableOption({ name, value })}
                >
                  {value}
                </option>
              ))}
            </Select>
          </FormControl>
        ))}
        <FormControl id="quantity" isRequired mb={4}>
          <FormLabel>{t("productForm.quantityLabel")}</FormLabel>
          <NumberInput
            variant="filled"
            min={1}
            max={10}
            defaultValue={quantityRef.current}
            onChange={(_, valueAsNumber) => handleQuantityChange(valueAsNumber)}
            isDisabled={!shopifyProduct.availableForSale}
          >
            <NumberInputField type="number" />
            <NumberInputStepper>
              <NumberIncrementStepper
                _active={{ bg: "purple.300" }}
                children={<AddIcon />}
              />
              <NumberDecrementStepper
                _active={{ bg: "pink.300" }}
                children={<MinusIcon />}
              />
            </NumberInputStepper>
          </NumberInput>
        </FormControl>
      </DarkMode>
      <Button
        leftIcon={<FiShoppingCart />}
        colorScheme="purple"
        type="submit"
        isLoading={isAddingToCart}
        loadingText={t("productForm.addingToCart")}
        mt={4}
        onClick={handleAddToCartClick}
        isDisabled={
          isAddingToCart ||
          !shopifyProduct.availableForSale ||
          !hasConfirmedAvailability
        }
      >
        {t("productForm.addToCart")}
      </Button>
    </>
  );
};

export default ProductForm;
