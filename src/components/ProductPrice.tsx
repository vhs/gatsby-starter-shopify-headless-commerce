import React, { FC } from "react";
import {
  Stat,
  StatNumber,
  StatHelpText,
  Text,
  StatLabel,
} from "@chakra-ui/core";

import { useTranslation } from "react-i18next";

import { formatCurrency } from "../utils/format";

interface ProductPriceProps {
  price: string;
  currencyCode: string;
  compareAtPrice?: string;
  labelText?: string;
  statNumberFontSize?: string;
}

const ProductPrice: FC<ProductPriceProps> = ({
  price,
  currencyCode,
  compareAtPrice,
  labelText,
  statNumberFontSize = "2xl",
}) => {
  const { i18n, t } = useTranslation();

  const getPrice = (price: string, currencyCode: string) =>
    formatCurrency(price, i18n.language, currencyCode);

  return (
    <Stat>
      {labelText && (
        <StatLabel fontSize="lg" display="inline">
          {labelText}
        </StatLabel>
      )}{" "}
      <StatNumber display="inline" fontSize={statNumberFontSize}>
        {compareAtPrice && (
          <Text as="s" color="gray.600" display="inline">
            {getPrice(compareAtPrice, currencyCode)}
          </Text>
        )}{" "}
        <Text color="purple.200" display="inline">
          {getPrice(price, currencyCode)}
        </Text>
      </StatNumber>
    </Stat>
  );
};

export default ProductPrice;
