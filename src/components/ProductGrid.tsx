import React, { FC } from "react";
import { useStaticQuery, graphql } from "gatsby";
import { Grid, Text } from "@chakra-ui/core";

import ProductCard, { ProductCardProps } from "./ProductCard";

interface ProductQueryValue {
  node: {
    id: string;
  } & ProductCardProps;
}

const ProductGrid: FC = () => {
  const {
    allShopifyProduct: { edges: shopifyProducts },
  } = useStaticQuery(productGridQuery);

  return (
    <Grid
      templateColumns={["repeat(1, 1fr)", "repeat(2, 1fr)", "repeat(3, 1fr)"]}
      gap={[6, 4]}
    >
      {shopifyProducts.length ? (
        shopifyProducts.map(({ node: { id, ...rest } }: ProductQueryValue) => (
          <ProductCard key={id} {...rest} />
        ))
      ) : (
        <Text>No products found!</Text>
      )}
    </Grid>
  );
};

export default ProductGrid;

const productGridQuery = graphql`
  query ProductGridQuery {
    allShopifyProduct(limit: 21, sort: { fields: [createdAt], order: DESC }) {
      edges {
        node {
          id
          handle
          title
          availableForSale
          images {
            localFile {
              childImageSharp {
                ...ProductImageFluid
              }
            }
          }
          variants {
            # compareAtPrice
            price
            priceV2 {
              amount
              currencyCode
            }
          }
          priceRange {
            minVariantPrice {
              amount
              currencyCode
            }
            maxVariantPrice {
              amount
            }
          }
        }
      }
    }
  }
`;
