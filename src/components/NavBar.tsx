import React, { FC } from "react";
import { Link as ReachLink, navigate } from "@reach/router";
import { createIcon, MoonIcon, SunIcon } from "@chakra-ui/icons";
import { FiShoppingCart as CartIcon } from "react-icons/fi";
import {
  Flex,
  Spacer,
  Heading,
  useColorModeValue,
  useColorMode,
  IconButton,
  Tooltip,
  Stack,
  Link,
  Box,
  Badge,
} from "@chakra-ui/core";

import { useCartCount } from "gatsby-theme-shopify-manager";
import { useTranslation } from "react-i18next";

import LanguageToggle from "./LanguageToggle";
const LogoIcon = createIcon({
  displayName: "LogoIcon",
  viewBox: "0 0 46 45",
  d: "M.708 45L23 .416 45.292 45H.708zM35 38L23 19 11 38h24z",
});

interface NavBarProps {
  siteTitle: string;
}

const NavBar: FC<NavBarProps> = ({ siteTitle }) => {
  const BrightnessIcon = useColorModeValue(MoonIcon, SunIcon);
  const { colorMode, toggleColorMode } = useColorMode();
  const cartCount = useCartCount();
  const { t } = useTranslation();

  return (
    <Flex align="center" justify="space-between" maxW="960px" margin="0 auto">
      <Link as={ReachLink} className="navlink" to="/">
        <Stack direction="row">
          <LogoIcon fontSize="3xl" color="whiteAlpha.900" />
          <Heading as="h1" fontSize="3xl" fontFamily="Crimson Text" ml={4}>
            {siteTitle}
          </Heading>
        </Stack>
      </Link>
      <Spacer />
      <Stack as="nav" direction="row" spacing={8} display={["none", "inherit"]}>
        <Link as={ReachLink} className="navlink" to="/" fontSize="xl">
          Home
        </Link>
        <Link
          as={ReachLink}
          disabled
          className="navlink"
          to="/collections/all"
          fontSize="xl"
          onClick={(event) => event.preventDefault()}
        >
          Catalog
        </Link>
      </Stack>
      <Spacer />
      <Stack spacing={2} direction="row">
        <LanguageToggle />
        <Tooltip
          hasArrow
          label={useColorModeValue(
            t("navbar.tooltip.reduceBrightness"),
            t("navbar.tooltip.increaseBrightness"),
          )}
          placement="left"
        >
          <IconButton
            color={useColorModeValue("red.500", "gray.600")}
            aria-label={t("navbar.button.changeBrightness")}
            isRound
            variant="ghost"
            fontSize="xl"
            icon={<BrightnessIcon />}
            onClick={toggleColorMode}
          />
        </Tooltip>
        <Box pos="relative">
          <IconButton
            color={cartCount ? "gray.400" : "gray.600"}
            aria-label={t("navbar.button.viewShoppingCart")}
            isRound
            variant="ghost"
            fontSize="xl"
            icon={<CartIcon />}
            onClick={() => navigate("/cart")}
          />
          <Badge
            pos="absolute"
            __css={{ pointerEvents: "none" }}
            variant="subtle"
            color={colorMode === "light" ? "purple.200" : "purple.400"}
            bg="whiteAlpha.200"
            borderRadius="full"
            px={2}
            top={-1}
            right={-1}
          >
            {cartCount}
          </Badge>
        </Box>
      </Stack>
    </Flex>
  );
};

export default NavBar;
