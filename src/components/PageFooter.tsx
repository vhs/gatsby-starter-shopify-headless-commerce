import React, { FC } from "react";
import { Link as ReachLink } from "@reach/router";
import { FaTwitter, FaInstagram, FaEnvelope } from "react-icons/fa";
import {
  Box,
  Stack,
  HStack,
  Heading,
  Link,
  Text,
  Spacer,
  Center,
} from "@chakra-ui/core";

import { useTranslation } from "react-i18next";
import { useMdx } from "../hooks/useMdx";
import { useSiteMetadata } from "../hooks/useSiteMetadata";
import { PaymentMethods } from "./ui";

const PageFooter: FC = () => {
  const {
    social: { instagramUsername, twitterUsername },
    author,
    copyright,
  } = useSiteMetadata();
  const { t } = useTranslation();
  const allMdx = useMdx();

  return (
    <Box as="footer" mt={4} maxW="960px" margin={[4, "0 auto"]}>
      <Stack direction={["column", "row"]} align="top" spacing={4} mt={12}>
        <Stack flexGrow={1} mb={8}>
          <Heading
            fontSize="md"
            fontWeight={400}
            textTransform="uppercase"
            borderBottom="1px solid #69696960"
            paddingBottom={2}
          >
            {t("pageFooter.policies.heading")}
          </Heading>
          {allMdx
            .filter((mdx) => mdx.slug.startsWith("policies"))
            .map(({ id, frontmatter, slug }) => (
              <Link key={id} as={ReachLink} to={`/${slug}`}>
                {frontmatter.title}
              </Link>
            ))}
        </Stack>
        <Stack flexGrow={1} mb={8}>
          <Heading
            fontSize="md"
            fontWeight={400}
            textTransform="uppercase"
            borderBottom="1px solid #69696960"
            paddingBottom={2}
          >
            {t("pageFooter.services.heading")}
          </Heading>
          <Link as={ReachLink} to="/">
            {t("pageFooter.services.ourProducts")}
          </Link>
        </Stack>
        <Stack flexGrow={1} mb={8}>
          <Heading
            fontSize="md"
            fontWeight={400}
            textTransform="uppercase"
            borderBottom="1px solid #69696960"
            paddingBottom={2}
          >
            {t("pageFooter.connect.heading")}
          </Heading>
          <Text maxW="300px">{t("pageFooter.connect.blurb")}</Text>
          <HStack spacing={4}>
            <Link
              isExternal
              href={`https://instagram.com/${instagramUsername.replace(
                "@",
                "",
              )}`}
            >
              <Box display="inline" as={FaInstagram} mr={1} color="gray.100" />
              {` `}
              Instagram
            </Link>
            <Link
              isExternal
              href={`https://twitter.com/${twitterUsername.replace("@", "")}`}
            >
              <Box display="inline" as={FaTwitter} mr={1} color="gray.100" />
              {` `}
              Twitter
            </Link>
            <Link isExternal href={`mailto:${author}`}>
              <Box display="inline" as={FaEnvelope} mr={1} color="gray.100" />
              {` `}
              Email
            </Link>
          </HStack>
        </Stack>
      </Stack>

      <Stack
        direction="row"
        display={["none", "flex"]}
        py={6}
        alignItems="flex-end"
      >
        <Text fontSize="sm">
          {copyright} • {t("pageFooter.madeIn")}
        </Text>
        <Spacer />
        <PaymentMethods
          justify="flex-end"
          spacing={1}
          maxW={["400px", "400px", "600px"]}
        />
      </Stack>
      <Center textAlign="center">
        <Stack display={["flex", "none"]} spacing={12} my={8}>
          <PaymentMethods justify="center" spacing={1} />
          <Text fontSize="sm">
            {copyright} • {t("pageFooter.madeIn")}
          </Text>
        </Stack>
      </Center>
    </Box>
  );
};

export default PageFooter;
