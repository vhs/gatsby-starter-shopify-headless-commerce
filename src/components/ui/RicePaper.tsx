import React, { FC } from "react";
import { Box, BoxProps } from "@chakra-ui/core";

const RicePaper: FC<BoxProps> = ({ children, ...boxProps }) => (
  <Box
    p={2}
    style={{
      backdropFilter: "brightness(0.7) saturate(80%)",
    }}
    boxShadow="lg"
    width="100%"
    display="table"
    {...boxProps}
  >
    {children}
  </Box>
);

export default RicePaper;
