import React, { FC } from "react";
import { graphql, useStaticQuery } from "gatsby";
import { navigate } from "@reach/router";
import { FluidObject } from "gatsby-image";
import { ArrowForwardIcon } from "@chakra-ui/icons";
import {
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  DarkMode,
  Divider,
  Button,
} from "@chakra-ui/core";

import { useTranslation } from "react-i18next";

import LineItem, { ILineItem } from "./LineItem";

interface CartItemsQuery {
  allShopifyProductVariant: {
    nodes: {
      shopifyId: string;
      image: {
        localFile: {
          childImageSharp: {
            fluid: FluidObject;
          };
        };
      };
    }[];
  };
}

interface CartItemsProps {
  lineItems: ILineItem[];
}

const CartItems: FC<CartItemsProps> = ({ lineItems }) => {
  const { t } = useTranslation();
  const {
    allShopifyProductVariant: { nodes: variants },
  } = useStaticQuery<CartItemsQuery>(cartItemsQuery);

  const getImageFluidForVariant = (variantId: string) =>
    variants.find((variant) => variant.shopifyId === variantId)?.image.localFile
      .childImageSharp.fluid;

  return (
    <>
      {lineItems.length ? (
        <>
          <Divider borderColor="gray.500" mb={2} />
          {lineItems.map((lineItem) => (
            <LineItem
              key={lineItem.id}
              imageFluid={getImageFluidForVariant(lineItem.variant.id)}
              lineItem={lineItem}
            />
          ))}
          <Divider borderColor="gray.500" mt={2} />
        </>
      ) : (
        <DarkMode>
          <Alert status="info" variant="left-accent">
            <AlertIcon />
            <AlertTitle mr={2}>{t("cart.emptyAlert.title")}</AlertTitle>
            <AlertDescription>
              {t("cart.emptyAlert.description")}
            </AlertDescription>
          </Alert>
          <Button mt={4} onClick={() => navigate("/")}>
            Continue Shopping <ArrowForwardIcon ml={2} />
          </Button>
        </DarkMode>
      )}
    </>
  );
};

export default CartItems;

const cartItemsQuery = graphql`
  query CartItemsQuery {
    allShopifyProductVariant {
      nodes {
        shopifyId
        image {
          localFile {
            childImageSharp {
              ...ProductImageThumbFluid
            }
          }
        }
      }
    }
  }
`;
