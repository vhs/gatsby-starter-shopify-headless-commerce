import React, { FC, Fragment, useState } from "react";
import GatsbyImage, { FluidObject } from "gatsby-image";
import {
  Heading,
  Button,
  Box,
  Flex,
  Spacer,
  DarkMode,
  Stat,
  StatLabel,
  StatNumber,
  StatGroup,
} from "@chakra-ui/core";

import { useRemoveItemFromCart } from "gatsby-theme-shopify-manager";
import { useTranslation } from "react-i18next";

import { formatCurrency } from "../../utils/format";

export interface ILineItem {
  id: string;
  title: string;
  quantity: number;
  variant: {
    id: string;
    selectedOptions: {
      name: string;
      value: string;
    }[];
    priceV2: {
      amount: string;
      currencyCode: string;
    };
  };
}

interface LineItemProps {
  imageFluid: FluidObject | undefined;
  lineItem: ILineItem;
}

const LineItem: FC<LineItemProps> = ({ imageFluid, lineItem }) => {
  const {
    variant: {
      selectedOptions,
      id: variantId,
      priceV2: { amount, currencyCode },
    },
  } = lineItem;

  const { t, i18n } = useTranslation();
  const removeItemFromCart = useRemoveItemFromCart();
  const [isRemovingFromCart, setIsRemovingFromCart] = useState(false);

  const handleRemoveClick = async () => {
    try {
      setIsRemovingFromCart(true);
      await removeItemFromCart(variantId);
    } catch {
      setIsRemovingFromCart(false);
      throw new Error(
        `There was a problem removing ${lineItem.title} from your cart.`,
      );
    }
  };

  const getPrice = (price: string) =>
    formatCurrency(price, i18n.language, currencyCode);

  const lineItemUnitPrice = getPrice(amount);
  const lineItemCost = getPrice(String(lineItem.quantity * Number(amount)));

  return (
    <Flex align="center" flexWrap="wrap">
      <Box
        as={GatsbyImage}
        width={24}
        height={24}
        fluid={imageFluid}
        alt={lineItem.title}
        boxShadow="sm"
        my={2}
      />
      <Box p={4}>
        <Heading fontSize="xl">{lineItem.title}</Heading>
        {selectedOptions.length && (
          <Box as="dl">
            {selectedOptions.map((selectedOption) => (
              <Fragment key={selectedOption.name}>
                <Box as="dt" display="inline-block" _after={{ content: `":"` }}>
                  {t(
                    `__GRAPHQL__.options.${selectedOption.name.toLowerCase()}`,
                  )}
                </Box>
                <Box as="dd" ml={1} display="inline-block">
                  {selectedOption.value}
                </Box>
              </Fragment>
            ))}
          </Box>
        )}
      </Box>
      <Spacer />
      <StatGroup
        display={["none", "inherit"]}
        minW={350}
        textAlign="right"
        mr={10}
      >
        <Stat>
          <StatLabel>{t("cart.lineItem.quantity")}</StatLabel>
          <StatNumber>{lineItem.quantity}</StatNumber>
        </Stat>
        <Stat>
          <StatLabel>{t("cart.lineItem.unitPrice")}</StatLabel>
          <StatNumber>{lineItemUnitPrice}</StatNumber>
        </Stat>
        <Stat>
          <StatLabel>{t("cart.lineItem.cost")}</StatLabel>
          <StatNumber>{lineItemCost}</StatNumber>
        </Stat>
      </StatGroup>
      <DarkMode>
        <Button isLoading={isRemovingFromCart} onClick={handleRemoveClick}>
          {t("cart.buttonRemove")}
        </Button>
      </DarkMode>
    </Flex>
  );
};

export default LineItem;
