import React, { FC } from "react";
import { motion } from "framer-motion";
import { Link as ReachLink } from "@reach/router";
import GatsbyImage, { FluidObject } from "gatsby-image";
import { Stack, Heading, Box, Link, Badge } from "@chakra-ui/core";

import { useTranslation } from "react-i18next";

import { RicePaper } from "./ui";
import ProductPrice from "./ProductPrice";
import { formatCurrency } from "../utils/format";
import { IShopifyProduct } from "./ProductForm";

export interface ProductCardProps {
  handle: string;
  title: string;
  availableForSale: boolean;
  images: {
    localFile: {
      childImageSharp: {
        fluid: FluidObject;
      };
    };
  }[];
  variants: {
    priceV2: {
      amount: string;
      currencyCode: string;
    };
    compareAtPrice: string;
    weight: number;
    weightUnit: string;
  }[];
  priceRange: {
    minVariantPrice: {
      amount: string;
      currencyCode: string;
    };
    maxVariantPrice: {
      amount: string;
    };
  };
}

const MotionBox = motion.custom(Box);

const ProductCard: FC<ProductCardProps> = ({
  handle,
  title,
  availableForSale,
  variants,
  variants: [firstVariant],
  images: [firstImage],
  priceRange,
}) => {
  const { t } = useTranslation();

  const fromPriceVariant = (() =>
    variants.reduce((lowest, current) =>
      Number(current.priceV2.amount) / current.weight <
      Number(lowest.priceV2.amount) / lowest.weight
        ? current
        : lowest,
    ))();

  return (
    <Stack textAlign="center" position="relative">
      <RicePaper borderRadius="lg">
        {firstImage?.localFile && (
          <MotionBox whileHover={{ scale: 1.02 }} whileTap={{ scale: 0.98 }}>
            <Box
              as={ReachLink}
              to={`/product/${handle}`}
              boxShadow="md"
              outline="none"
              tabIndex={-1}
            >
              <Box
                as={GatsbyImage}
                fluid={firstImage.localFile.childImageSharp.fluid}
                alt={handle}
                borderRadius="md"
              />
            </Box>
          </MotionBox>
        )}
        <Box position="absolute" top={3} left={4}>
          {!availableForSale && (
            <Badge variant="outline" pointerEvents="none">
              {t("badges.soldOut")}
            </Badge>
          )}
          {availableForSale && firstVariant.compareAtPrice && (
            <Badge variant="solid" colorScheme="red">
              {t("badges.sale")}
            </Badge>
          )}
        </Box>
        <Box p={2}>
          <Heading as="h2" size="md" my={2}>
            <Link
              as={ReachLink}
              className="navlink"
              to={`/product/${handle}`}
              p={2}
            >
              {title}
            </Link>
          </Heading>
          <ProductPrice
            labelText={t("productCard.statFrom")}
            price={fromPriceVariant.priceV2.amount}
            currencyCode={fromPriceVariant.priceV2.currencyCode}
            statNumberFontSize="xl"
          />
        </Box>
      </RicePaper>
    </Stack>
  );
};

export default ProductCard;
