import React, { FC } from "react";
import { IconButton, Tooltip } from "@chakra-ui/core";
import { MdLanguage } from "react-icons/md";

import { useTranslation } from "react-i18next";
import { useSiteMetadata } from "../hooks/useSiteMetadata";
import { Locale } from "../utils/language";

const LanguageToggle: FC = () => {
  const { translations } = useSiteMetadata() as {
    translations: Array<keyof typeof Locale>;
  };
  const { t, i18n } = useTranslation();
  const locale = i18n.language as keyof typeof Locale;
  const toggleLanguage = () => {
    locale === "en" ? i18n.changeLanguage("id") : i18n.changeLanguage("en");
  };

  return (
    <Tooltip
      hasArrow
      label={t("languageToggle.tooltip.switchToLanguage", {
        language: locale === "en" ? Locale["id"] : Locale["en"],
      })}
      placement="left"
    >
      <IconButton
        disabled={!translations.some((locale) => locale === "id")}
        color="gray.600"
        aria-label={t("languageToggle.button.changeLanguages")}
        isRound
        variant="ghost"
        fontSize="xl"
        icon={<MdLanguage />}
        onClick={toggleLanguage}
      />
    </Tooltip>
  );
};

export default LanguageToggle;
