import React, { PropsWithChildren, FC, ReactNode } from "react";
import { MDXProvider } from "@mdx-js/react";
import * as Chakra from "@chakra-ui/core";

import Layout from "./Layout";
import SEO from "./SEO";

const MDXLayout: FC<PropsWithChildren<ReactNode>> = ({
  children,
  pageContext,
}) => (
  <MDXProvider
    components={{
      h1: (props) => (
        <Chakra.Heading
          {...props}
          as="h1"
          fontWeight="400"
          fontSize="3xl"
          mt={4}
          mb={2}
        />
      ),
      h2: (props) => (
        <Chakra.Heading
          {...props}
          as="h2"
          fontWeight="400"
          fontSize="2xl"
          mt={4}
          mb={2}
        />
      ),
      h3: (props) => (
        <Chakra.Heading {...props} as="h3" fontSize="xl" mt={4} mb={2} />
      ),
      p: (props) => <Chakra.Text {...props} fontSize="md" mb={4} />,
      ul: (props) => (
        <Chakra.Box
          as="ul"
          {...props}
          ml={4}
          mb={4}
          fontFamily="Source Sans Pro"
        />
      ),
    }}
  >
    <Layout>
      <SEO
        title={pageContext.frontmatter.title}
        robots={
          pageContext.frontmatter.robots
            ? pageContext.frontmatter.robots
            : "index, follow"
        }
      />
      {children}
    </Layout>
  </MDXProvider>
);

export default MDXLayout;
