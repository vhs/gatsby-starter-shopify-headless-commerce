import React, { FC, useState } from "react";
import {
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  CloseButton,
} from "@chakra-ui/core";

interface ErrorAlertProps {
  message: string;
}

const ErrorAlert: FC<ErrorAlertProps> = ({ message }) => {
  const [wasDismissed, setWasDismissed] = useState(false);

  const handleCloseClick = () => {
    setWasDismissed(true);
  };

  return !wasDismissed ? (
    <Alert status="error" mb={4}>
      <AlertIcon />
      <AlertTitle mr={2}>Oops...</AlertTitle>
      <AlertDescription>{message}</AlertDescription>
      <CloseButton
        position="absolute"
        right="8px"
        top="8px"
        onClick={handleCloseClick}
      />
    </Alert>
  ) : null;
};

export default ErrorAlert;
