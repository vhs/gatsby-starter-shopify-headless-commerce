import React, { FC } from "react";
import { Box } from "@chakra-ui/core";

import { useSiteMetadata } from "../hooks/useSiteMetadata";
import NavBar from "./NavBar";
import SEO from "./SEO";
import PageFooter from "./PageFooter";
import { RicePaper } from "./ui";

const Layout: FC = ({ children }) => {
  const { title, image } = useSiteMetadata();
  return (
    <>
      <SEO />
      <Box
        as="header"
        _before={{
          content: `""`,
          position: "fixed",
          width: "100%",
          height: "100vh",
          opacity: 0.2,
          backgroundImage: `url(${image})`,
          backgroundSize: "cover",
          zIndex: -1,
        }}
      >
        <RicePaper p={4}>
          <NavBar siteTitle={title} />
        </RicePaper>
      </Box>
      <Box as="main" mt={6} maxW="960px" marginX={[4, 4, 4, "auto"]}>
        {children}
      </Box>
      <RicePaper p={4} mt={16}>
        <PageFooter />
      </RicePaper>
    </>
  );
};

export default Layout;
