import { useStaticQuery, graphql } from "gatsby";

interface SiteMetadata {
  baseUrl: string;
  translations: string[];
  lang: string;
  title: string;
  titleTemplate: string;
  description: string;
  author: string;
  copyright: string;
  image: string;
  social: {
    twitterUsername: string;
    instagramUsername: string;
    telegramChannel: string;
  };
  robots: string;
  keywords: string[];
  forms: {
    [handle: string]: string;
  };
}

export const useSiteMetadata = (): SiteMetadata => {
  const { site } = useStaticQuery(
    graphql`
      query SiteMetadata {
        site {
          siteMetadata {
            baseUrl
            translations
            lang
            title
            titleTemplate
            description
            author
            copyright
            image
            social {
              twitterUsername
              instagramUsername
              telegramChannel
            }
            robots
            keywords
            forms {
              emailSubscription
            }
          }
        }
      }
    `,
  );
  return site.siteMetadata;
};
