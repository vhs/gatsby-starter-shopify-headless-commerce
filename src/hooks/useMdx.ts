import { useStaticQuery, graphql } from "gatsby";

interface MdxDocument {
  id: string;
  frontmatter: {
    title: string;
  };
  slug: string;
}

interface AllMdxQuery {
  allMdx: {
    edges: {
      node: MdxDocument;
    }[];
  };
}

export const useMdx = () => {
  const { allMdx } = useStaticQuery<AllMdxQuery>(
    graphql`
      query AllMdxQuery {
        allMdx(sort: { fields: frontmatter___title }) {
          edges {
            node {
              id
              frontmatter {
                title
              }
              slug
            }
          }
        }
      }
    `,
  );
  return allMdx.edges.map((edge) => edge.node);
};
