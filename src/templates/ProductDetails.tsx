import React, { FC } from "react";
import { graphql } from "gatsby";
import GatsbyImage, { FluidObject } from "gatsby-image";
import { Heading, Badge, Grid, Box } from "@chakra-ui/core";

import { useTranslation } from "react-i18next";

import Layout from "../components/Layout";
import { RicePaper } from "../components/ui";
import SEO from "../components/SEO";
import ProductForm, { IShopifyProduct } from "../components/ProductForm";

interface ProductDetailsProps {
  data: {
    shopifyProduct: {
      handle: string;
      title: string;
      description: string;
      descriptionHtml: string;
      images: {
        id: string;
        localFile: {
          childImageSharp: {
            fluid: FluidObject;
          };
        };
      }[];
    } & IShopifyProduct;
  };
}

const ProductDetails: FC<ProductDetailsProps> = ({
  data: { shopifyProduct },
}) => {
  const [firstImage] = shopifyProduct.images;
  const { t } = useTranslation();

  return (
    <Layout>
      <SEO
        title={shopifyProduct.title}
        description={shopifyProduct.description}
      />
      <Grid
        templateColumns={["repeat(1, 1fr)", "repeat(1, 1fr)", "repeat(2, 1fr)"]}
        gap={6}
      >
        {shopifyProduct.images
          .filter((image) => image === firstImage)
          .map((image) => (
            <RicePaper key={image.id} borderRadius="lg">
              {image.localFile && (
                <Box
                  as={GatsbyImage}
                  fluid={image.localFile.childImageSharp.fluid}
                  alt={shopifyProduct.handle}
                  borderRadius="md"
                  boxShadow="md"
                ></Box>
              )}
            </RicePaper>
          ))}
        <Box mb={6}>
          <Heading fontFamily="Crimson Text" mb={4}>
            {shopifyProduct.title}
          </Heading>
          <Box
            dangerouslySetInnerHTML={{ __html: shopifyProduct.descriptionHtml }}
            className="product-description"
          />
          {!shopifyProduct.availableForSale && (
            <Badge variant="outline">{t("badges.soldOut")}</Badge>
          )}
          <ProductForm shopifyProduct={shopifyProduct} />
        </Box>
      </Grid>
    </Layout>
  );
};

export default ProductDetails;

export const query = graphql`
  query($handle: String!) {
    shopifyProduct(handle: { eq: $handle }) {
      handle
      title
      description
      descriptionHtml
      shopifyId
      availableForSale
      options {
        id
        name
        values
      }
      variants {
        title
        priceV2 {
          amount
          currencyCode
        }
        # compareAtPrice
        availableForSale
        shopifyId
        selectedOptions {
          name
          value
        }
        weight
        weightUnit
      }
      images {
        id
        localFile {
          childImageSharp {
            ...ProductImageFluid
          }
        }
      }
    }
  }
`;
