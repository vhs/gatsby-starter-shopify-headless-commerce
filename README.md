# gatsby-starter-shopify-headless-commerce

> Gatsby starter for headless Shopify commerce sites.

Build great-looking Shopify headless commerce sites with [React](https://reactjs.org/) and [TypeScript](https://www.typescriptlang.org/) using [Gatsby](https://www.gatsbyjs.org/).

## Screenshot

![screenshot](docs/screenshot.png)

## Demo

An online demo of this project is available at https://gatsby-starter-shopify-headless-commerce.vercel.app/. To preview the Coming Soon page append `password` to the URL.

## Included

- Built from scratch using a single codebase extending upon [Gatsby Starter i18n React i18next](https://vhs.codeberg.page/code/gatsby-starter-i18n-react-i18next/).
- Clean and well-tested shopping experience using [Gatsby Theme Shopify Manager](https://gatsbythemeshopifymanager.com/) data theme.
- Shopify Shop policy pages hosted locally using [MDX](https://mdxjs.com/) markdown.
- Dynamic inventory checking verifies limited items are in stock before purchase.
- Coming Soon page with attractive [GetForm](https://getform.io/) launch notice email sign-up form.
- [Chakra UI](https://chakra-ui.com/) component library and design system with persistent light/dark color mode toggle.
- [Framer Motion](https://www.framer.com/motion/) to create stunning visual effects.
- [Jest](https://jestjs.io/) testing framework using [Testing Library](https://testing-library.com/) with integrated [test-utils](https://testing-library.com/docs/react-testing-library/setup#configuring-jest-with-test-utils) and example tests.
- Functional [React](https://reactjs.org/) component examples written in [TypeScript](https://www.typescriptlang.org/) (strict) and using [React Hooks](https://reactjs.org/docs/hooks-reference.html) API.
- Internationalization with [react-i18next](https://react.i18next.com/) and button to toggle between English and Bahasa Indonesia.
- Automatic, in-browser language detection via integrated [i18next plugin](https://github.com/i18next/i18next-browser-languageDetector).
- Custom SVG logo icon borrowed from [After Dark](https://vhs.codeberg.page/code/after-dark/) theme for Hugo static site generator.

## Requirements

- Node
- TypeScript
- Yarn

## Installation

1. Copy source code to your machine.
2. Run `yarn` to install dependencies.
3. Copy `.env.example` into `.env` file locally.

## Usage

- Run `yarn dev` to start a server for development.
- Run `yarn test` to run unit tests.
- Run `yarn type-check` to run type checker.
- Run `yarn build` to build site for production.
- Run `yarn serve` to view production build.
- Run `yarn analyze` to inspect JS bundle sizes.
- Run `yarn deploy` to deploy to S3 with server-side redirect to Coming Soon page containing email launch sign-up and password-based login.

Note only `SHOPIFY` env variables are required to get started. For deployments to S3 you'll need to create a bucket following Gatsby instructions. Coming Soon page redirect should also work on other providers. Some purchasing controls may not work as expected until you set-up your own inventory.

For Auth0 integration on the password page you'll need to create an Auth0 account and complete your configuration using the [Auth0 instructions](https://github.com/auth0/auth0-react#getting-started). A word of caution: static pages are not protected by Auth0. You'll need to set-up your own [client-only routes](https://www.gatsbyjs.com/docs/client-only-routes-and-user-authentication/) for that.

Finally, there's a hard-limit on the number of items which display in the Product Grid. This can be changed by adjusting the GraphQL query filters set in the Product Grid.

## Limitations

If you use differing product images per variant you'll need to add the code to display those yourself on the Product Details Page.

## Troubleshooting

```
"gatsby-source-shopify" threw an error while running the sourceNodes lifecycle:
TimeoutError: Timeout awaiting 'request' for 30000ms
```

If you're on a slow internet connection Shopify may not have a chance to download all assets fast enough and will throw an error curing compilation. If that's the case try configuring the Shopify plugin to increase the threshhold or try rebuilding a few times to build up the Gatsby cache so you can get a clean build with all products.

## Rights

Copyright © 2020 VHS <0xc000007b@tutanota.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

This work incorporates fonts covered under copyright by Adobe and Sebastian Kosch. See the fonts directory for copyright and permissions notices.
